package MobileLegends;

public class Skill2 implements  Action{

    String actionMsg="";
    Double dmg = 0d;

    Skill2(String actionMessage, double baseDamage){
        this.actionMsg = actionMessage;
        this.dmg = baseDamage;
    }

    @Override
    public void Action(String actionMessage, double baseDamage) {

    }

    @Override
    public String getMessage() {
        return actionMsg;
    }

    @Override
    public double getDouble() {
        return dmg;
    }


}
