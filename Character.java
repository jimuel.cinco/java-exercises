package MobileLegends;

import javax.naming.directory.BasicAttribute;

public class Character {
    private String name;
    private Action basicAttack;
    private Action skill1;
    private Action skill2;


    public Character(String name, Action basicAttack, Action skill1, Action skill2){
        this.name = name;
        this.basicAttack = basicAttack;
        this.skill1 = skill1;
        this.skill2 = skill2;
    }

    public void performAction(String typeofAction, Character targetCharacter, Character initCharacter){


        String completeAction = initCharacter.name+" ";
        String actionname = "";
        double dmg = 0d;

        if(typeofAction.equals("basic")){
            actionname =  initCharacter.basicAttack.getMessage();
            dmg = initCharacter.basicAttack.getDouble();
        }
        if(typeofAction.equals("skill1")){
            actionname =  initCharacter.skill1.getMessage();
            dmg = initCharacter.skill1.getDouble();
        }

        if(typeofAction.equals("skill2")){
            actionname =  initCharacter.skill2.getMessage();
            dmg = initCharacter.skill2.getDouble();
        }

        completeAction = completeAction + actionname + targetCharacter.name + " for "+dmg+ " damage!";
        System.out.println(completeAction);








    }


    public static void main(String[] args) {
        Action jawheadBasic = new BasicAttack("shoot missiles at ",10);
        Action jawheadSkill1 = new Skill1("flings ",50);
        Action jawheadSkill2 = new Skill2("locks onto and charges at ",80);


        Character jawHead  = new Character("Jawhead", jawheadBasic, jawheadSkill1, jawheadSkill2);


        Action chouBasic = new BasicAttack("punches ",11);
        Action chouSkill1 = new Skill1("dashes towards at ",65);
        Action chouSkill2 = new Skill2("does a redhouse kick at ",75);

        Character chou = new Character("Chou",chouBasic,chouSkill1,chouSkill2);

        jawHead.performAction("skill1",chou, jawHead);
        chou.performAction("basic",jawHead, chou);




    }
}
