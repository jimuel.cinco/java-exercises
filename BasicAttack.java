package MobileLegends;

public class BasicAttack implements  Action{

    String actionMsg="";
    Double dmg = 0d;
    BasicAttack(String actionMessage, double baseDamage){
        this.actionMsg = actionMessage;
        this.dmg = baseDamage;
    }

    @Override
    public void Action(String actionMessage, double baseDamage) {

    }

    @Override
    public String getMessage() {
        return actionMsg;
    }

    @Override
    public double getDouble() {
        return dmg;
    }
}
