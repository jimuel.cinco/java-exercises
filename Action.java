package MobileLegends;

public interface Action {


    void   Action(String actionMessage, double baseDamage);
    String getMessage();
    double getDouble();

}
