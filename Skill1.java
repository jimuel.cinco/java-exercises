package MobileLegends;



public class Skill1 implements  Action{

    String actionMsg="";
    Double dmg = 0d;

    Skill1(String actionMessage, double baseDamage){
        this.actionMsg = actionMessage;
        this.dmg = baseDamage;
    }

    @Override
    public void Action(String actionMessage, double baseDamage) {
    }

    @Override
    public String getMessage() {
        return actionMsg;
    }

    @Override
    public double getDouble() {
        return dmg;
    }


}
